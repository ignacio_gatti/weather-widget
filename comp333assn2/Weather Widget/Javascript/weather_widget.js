/*
 * Constructor function for a WeatherWidget instance.
 * 
 * container_element : a DOM element inside which the widget will place its UI
 *
 */
function WeatherWidget(container_element) {

    //declare the data properties of the object 

    var _list = [];
    var _request;
    var _currentSortOrder = 1;

    var _ui = {
        sortByName: null,
        sortByTemp: null,
        
        nameEntry: document.createElement("select"),
        container: null,
        toolbar: null,
        list: null,
        titlebar: null,
		
        getTown: document.createElement("button"),
        infoShow: null,

    };

    //declare an inner object literal to represent the widget's UI


    //write a function to create and configure the DOM elements for the UI
    var _createUI = function(container) {

        _ui.container = container;
        //_ui.container = container_element;
        _ui.container.className = "monitor";
        _ui.container.id = "draggable";
        _ui.titlebar = document.createElement("div");
        _ui.titlebar.className = "title";
        _ui.titlebar.label = document.createElement("span");
        _ui.titlebar.label.innerHTML = "Weather Details";
        _ui.titlebar.appendChild(_ui.titlebar.label);

        _ui.infoShow = document.createElement("div");
        _ui.infoShow.id = "infoShow";
        _ui.infoShow.innerHTML = "Click Above to See Information";

        _ui.toolbar = document.createElement("div");
        

        _ui.nameEntry.id = "nameEntry";
        var opt1 = document.createElement("option");
        opt1.text = "Hamilton";
        _ui.nameEntry.add(opt1);

        var opt2 = document.createElement("option");
        opt2.text = "Auckland";
        _ui.nameEntry.add(opt2);

        var opt3 = document.createElement("option");
        opt3.text = "Christchurch";
        _ui.nameEntry.add(opt3);

        var opt4 = document.createElement("option");
        opt4.text = "Dunedin";
        _ui.nameEntry.add(opt4);

        var opt5 = document.createElement("option");
        opt5.text = "Tauranga";
        _ui.nameEntry.add(opt5);

        var opt6 = document.createElement("option");
        opt6.text = "Wellington";
        _ui.nameEntry.add(opt6);


        _ui.sortByName = document.createElement("button");
        
        
        _ui.sortByName.id = "sortByName";
        _ui.sortByName.name = "sortBy";
        _ui.sortByName.innerHTML = "Show All By Name";
        _ui.sortByName.onclick = sortByName;

        _ui.sortByTemp = document.createElement("button");
        _ui.sortByTemp.name = "sortBy";
        _ui.sortByTemp.id = "sortByTemp";
       
        _ui.sortByTemp.innerHTML = "Show All By Temperature";
        _ui.sortByTemp.onclick = sortByTemperature;
		_ui.getTown.innerHTML = "Get Town Info";
        _ui.getTown.onclick = getTownInfo;
		
        
		

        _ui.list = document.createElement("div");
		_ui.toolbar.appendChild(_ui.nameEntry);
		_ui.toolbar.appendChild(_ui.sortByName);
        _ui.toolbar.appendChild(_ui.sortByTemp);

        _ui.container.appendChild(_ui.titlebar);
        _ui.container.appendChild(_ui.toolbar);
        _ui.container.appendChild(_ui.list);
        _ui.container.appendChild(_ui.getTown);
        _ui.container.appendChild(_ui.infoShow);

       

        function getTownInfo() {

            var name_of_town = _ui.nameEntry[_ui.nameEntry.selectedIndex].innerHTML;

            var url = "weather.php";
            var params = '?name_of_town=' + name_of_town;
            var target = document.getElementById("infoShow");
            var xhr = new XMLHttpRequest();
			
            xhr.open('GET', url + params, true);




            xhr.onreadystatechange = function() {
                console.log('readyState: ' + xhr.readyState);
                
                if (xhr.readyState == 2) {
                    target.innerHTML = 'Loading...';
                }
                if (xhr.readyState == 4 && xhr.status == 200) {
                    var jsonResponse = JSON.parse(xhr.responseText) ;
					var t = jsonResponse[0].town;
					var o = jsonResponse[0].outlook;
					var min = jsonResponse[0].min_temp;
					var max = jsonResponse[0].max_temp;

					target.innerHTML = t + "   outlook: " + o + "|   Min Temp: " + min + "|   Max Temp: " + max;

                }
            }

            xhr.send();
        }
		
		function sortByTemperature() {

            

            var url = "showAllWeather.php";
            
            var target = document.getElementById("infoShow");
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);



            xhr.onreadystatechange = function() {
                console.log('readyState: ' + xhr.readyState);
                console.log(xhr);
                if (xhr.readyState == 2) {
                    target.innerHTML = 'Loading...';
                }
                if (xhr.readyState == 4 && xhr.status == 200) {
                    
					
					var jsonResponse = JSON.parse(xhr.responseText);
					var t0 = jsonResponse[0].town;
					var o0 = jsonResponse[0].outlook;
					var min0 = jsonResponse[0].min_temp;
					var max0 = jsonResponse[0].max_temp;
					
					var t1 = jsonResponse[1].town;
					var o1 = jsonResponse[1].outlook;
					var min1 = jsonResponse[1].min_temp;
					var max1 = jsonResponse[1].max_temp;
					
					var t2 = jsonResponse[2].town;
					var o2 = jsonResponse[2].outlook;
					var min2 = jsonResponse[2].min_temp;
					var max2 = jsonResponse[2].max_temp;
					
					var t3 = jsonResponse[3].town;
					var o3 = jsonResponse[3].outlook;
					var min3 = jsonResponse[3].min_temp;
					var max3 = jsonResponse[3].max_temp;

					var t4 = jsonResponse[4].town;
					var o4 = jsonResponse[4].outlook;
					var min4 = jsonResponse[4].min_temp;
					var max4 = jsonResponse[4].max_temp;

					var t5 = jsonResponse[5].town;
					var o5 = jsonResponse[5].outlook;
					var min5 = jsonResponse[5].min_temp;
					var max5 = jsonResponse[5].max_temp;
					

					
					
					target.innerHTML = 
					
					t3 + "   outlook: " + o3 + "|   Min Temp: " + min3 + "|   Max Temp: " + max3 + "<br>" +
					t4 + "   outlook: " + o4 + "|   Min Temp: " + min4 + "|   Max Temp: " + max4 + "<br>" +
					t5 + "   outlook: " + o5 + "|   Min Temp: " + min5 + "|   Max Temp: " + max5 + "<br>" +
					t1 + "   outlook: " + o1 + "|   Min Temp: " + min1 + "|   Max Temp: " + max1 + "<br>" + 
					t2 + "   outlook: " + o2 + "|   Min Temp: " + min2 + "|   Max Temp: " + max2 + "<br>" +
					t0 + "   outlook: " + o0 + "|   Min Temp: " + min0 + "|   Max Temp: " + max0 + "<br>" ;
                }
            }

            xhr.send();
        }

		function sortByName() {

            

            var url = "showAllWeather.php";
            
            var target = document.getElementById("infoShow");
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);



            xhr.onreadystatechange = function() {
                console.log('readyState: ' + xhr.readyState);
                console.log(xhr);
                if (xhr.readyState == 2) {
                    target.innerHTML = 'Loading...';
                }
                if (xhr.readyState == 4 && xhr.status == 200) {
                    
					
					var jsonResponse = JSON.parse(xhr.responseText);
					var t0 = jsonResponse[0].town;
					var o0 = jsonResponse[0].outlook;
					var min0 = jsonResponse[0].min_temp;
					var max0 = jsonResponse[0].max_temp;
					
					var t1 = jsonResponse[1].town;
					var o1 = jsonResponse[1].outlook;
					var min1 = jsonResponse[1].min_temp;
					var max1 = jsonResponse[1].max_temp;
					
					var t2 = jsonResponse[2].town;
					var o2 = jsonResponse[2].outlook;
					var min2 = jsonResponse[2].min_temp;
					var max2 = jsonResponse[2].max_temp;
					
					var t3 = jsonResponse[3].town;
					var o3 = jsonResponse[3].outlook;
					var min3 = jsonResponse[3].min_temp;
					var max3 = jsonResponse[3].max_temp;

					var t4 = jsonResponse[4].town;
					var o4 = jsonResponse[4].outlook;
					var min4 = jsonResponse[4].min_temp;
					var max4 = jsonResponse[4].max_temp;

					var t5 = jsonResponse[5].town;
					var o5 = jsonResponse[5].outlook;
					var min5 = jsonResponse[5].min_temp;
					var max5 = jsonResponse[5].max_temp;
					

					
					
					target.innerHTML = 
					
					t1 + "   outlook: " + o1 + "|   Min Temp: " + min1 + "|   Max Temp: " + max1 + "<br>" +
					t2 + "   outlook: " + o2 + "|   Min Temp: " + min2 + "|   Max Temp: " + max2 + "<br>" +
					t3 + "   outlook: " + o3 + "|   Min Temp: " + min3 + "|   Max Temp: " + max3 + "<br>" +
					t0 + "   outlook: " + o0 + "|   Min Temp: " + min0 + "|   Max Temp: " + max0 + "<br>" +
					t4 + "   outlook: " + o4 + "|   Min Temp: " + min4 + "|   Max Temp: " + max4 + "<br>" +
					t5 + "   outlook: " + o5 + "|   Min Temp: " + min5 + "|   Max Temp: " + max5 + "<br>" ;



                }
            }

            xhr.send();
        }


        

    }




    //add any other methods required for the functionality


    /**
     * private method to intialise the widget's UI on start up
     * this method is complete
     */
    var _initialise = function(container_element) {
        _createUI(container_element);
    }


    /*********************************************************
     * Constructor Function for the inner WLine object to hold the 
     * full weather data for a town
     ********************************************************/

    var WLine = function(wtown, woutlook, wmin, wmax) {

        //declare the data properties for the object

        //declare an inner object literal to represent the widget's UI


        //write a function to create and configure the DOM elements for the UI
        var _createUI = function(container) {

        }

        //Add any remaining functions you need for the object

        //_createUI() method is called when the object is instantiated
        _createUI();



    }; //this is the end of the constructor function for the WLine object 


    //  _initialise method is called when a WeatherWidget object is instantiated
    _initialise(container_element);
}

//end of constructor function for WeatherWidget