USE master
GO
DROP DATABASE weatherDB

CREATE DATABASE weatherDB
Go

USE weatherDB
GO



CREATE TABLE weather (
  town varchar(50) NOT NULL,
  outlook varchar(30) NOT NULL,
  min_temp int NOT NULL DEFAULT '0',
  max_temp int NOT NULL DEFAULT '0',
  PRIMARY KEY (town)
) 
--ENGINE=MyISAM DEFAULT CHARSET=ascii;

--
-- Dumping data for table `weather`
--

INSERT INTO weather VALUES('Hamilton', 'sunny', 10, 28);
INSERT INTO weather VALUES('Auckland', 'cloudy', 8, 26);
INSERT INTO weather VALUES('Christchurch', 'fine', 9, 27);
INSERT INTO weather VALUES('Dunedin', 'foggy', 5, 23);
INSERT INTO weather VALUES('Tauranga', 'raining', 7, 24);
INSERT INTO weather VALUES('Wellington', 'windy', 7, 25);

SELECT * FROM weather